package com.example.app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }

    @FXML
    private Circle myCircle;
    private double x;
    private double y;

    public void dolje(ActionEvent e) {
        System.out.println("Dolje");
        myCircle.setCenterY(y+=5);
    }
    public void gore(ActionEvent e) {
        System.out.println("Gore");
        myCircle.setCenterY(y-=5);
    }
    public void desno(ActionEvent e) {
        System.out.println("Desno");
        myCircle.setCenterX(x+=5);
    }
    public void lijevo(ActionEvent e) {
        System.out.println("Lijevo");
        myCircle.setCenterX(x-=5);
    }

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void switchtoScene1(ActionEvent event) throws IOException{
        root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
        stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    public void switchtoScene2(ActionEvent event) throws IOException{
        root = FXMLLoader.load(getClass().getResource("scena2.fxml"));
        stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}